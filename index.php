<?php

require_once('src/configure.php');
require_once('src/authorization-check.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>5 друзей</title>
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<div class="background">
    <div class="welcome-background">
        <div class="welcome-header">
            <h2>5 Ваших друзей</h2>
        </div>
    <a class="authorization-button" href="
    <?php
    echo 'https://oauth.vk.com/authorize?client_id=' . API_ID .'&display=' . DISPLAY .'&redirect_uri=' .REDIRECT_URI .'&scope=' . SCOPE .'&response_type=code&v=' . API_VER . '&state=' . STATE;
    ?>
    ">Авторизация</a>
    </div>
</div>
</body>
</html>
