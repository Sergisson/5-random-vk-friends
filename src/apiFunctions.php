<?php

require_once('configure.php');

/********************************** Functions *********************************/

function getAccessToken()
{
    if ($_GET['state'] !== STATE) {
        header('Location: http://sergisson.byethost7.com/OAuthVK/error.php');
    }

    $params = array(
        'client_id'     => API_ID,
        'client_secret' => CLIENT_SECRET,
        'redirect_uri'  => REDIRECT_URI,
        'code' => $_GET['code'],
    );

    $url = 'https://oauth.vk.com/access_token';
    $url = $url . '?' . urldecode(http_build_query($params));
    $tokenInfo = file_get_contents($url);
    $tokenInfo = json_decode($tokenInfo, true);
    return $tokenInfo;
}

function userName($userId)
{
    $url = 'https://api.vk.com/method/users.get?user_id=' . $userId;
    $userInfo = file_get_contents($url);
    $userInfo = json_decode($userInfo, true);
    $userInfo = $userInfo['response'][0];
    $userInfo = $userInfo['first_name'] . ' ' . $userInfo['last_name'];
    return $userInfo;
}

function getFriendsId($userId)
{
    $url = 'https://api.vk.com/method/friends.get?user_id=' . $userId . '&count=' . FRIENDS_COUNT . '&order=random';
    $usersFriendsId = file_get_contents($url);
    $usersFriendsId = json_decode($usersFriendsId, true);
    $usersFriendsId = $usersFriendsId['response'];
    return $usersFriendsId;
}

function getFriendsNames($usersFriendsId)
{
    for ($count = 0; $count < FRIENDS_COUNT; $count++) {
        $url = 'https://api.vk.com/method/users.get?user_id=' . $usersFriendsId[$count];
        $friendInfo= file_get_contents($url);
        $friendInfo = json_decode($friendInfo, true);

        $usersFriendsInfo[$count]['firstName'] = $friendInfo['response'][0]['first_name'];
        $usersFriendsInfo[$count]['lastName'] = $friendInfo['response'][0]['last_name'];
    }

    return $usersFriendsInfo;
}

/********************************** Main code *********************************/

if (isset($_COOKIE["vkUserId"])) {
    $userId = $_COOKIE["vkUserId"];
} else {
    $tokenInfo = getAccessToken();
    $userId = $tokenInfo["user_id"];
    setcookie("vkUserId", $userId, strtotime(COOKIE_TIME));
}

$userName = userName($userId);
$usersFriendsId = getFriendsId($userId);
$usersFriendsInfo = getFriendsNames($usersFriendsId);
