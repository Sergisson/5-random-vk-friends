<?php

require_once('src/configure.php');
require_once('src/apiFunctions.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>5 друзей</title>
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<div class="background">
    <div class="welcome-background">
        <div class="welcome-header">
            <h2>
                <?php
                if ($userName) {
                    echo $userName;
                } else {
                    header('Location: http://sergisson.byethost7.com/OAuthVK/error.php');
                }
                ?>
            </h2>
        </div>
        <div class="friendsList">
            <?php
            if ($usersFriendsInfo) {
                for ($count = 0; $count < FRIENDS_COUNT; $count++) {
                    echo "<div class=\"friend\"> ";
                    echo $count + 1 . ' ' . $usersFriendsInfo[$count]['firstName'] . ' ' . $usersFriendsInfo[$count]['lastName'] . '<br>';
                    echo '</div>';
                }
            } else {
                header('Location: http://sergisson.byethost7.com/OAuthVK/error.php');
            }
            ?>
        </div>
    </div>
</div>


</body>
</html>
